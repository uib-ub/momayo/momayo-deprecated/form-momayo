# form-momayo

Repository for the Momayo ontology.

This is a repository that is used as a submodule. The data is in the parent repository and this `.pprj` file points to `../data/data.rdf-xml.owl`

Updated by cron job on server.

## Scripts

When saving the form Protégé will save the path to `data.rdf-xml.owl` as an abolute file path. One must find and replace this to `../data/data.rdf-xml.owl`.

```bash
npm run fix-path
```